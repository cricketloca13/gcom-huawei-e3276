# gcom-huawei-e3276

This package contains gcom script for the Huawei E3276 and E3272. It is used by the
[luci-app-ncm-status](https://gitlab.com/openwrt-wwan/luci-app-ncm-status)
package. This package is based on the version published at
https://sites.google.com/site/variousopenwrt/huawei-e3267, named
`ncm-huawei-e3276_1.0.1-1_ar71xx.ipk`.
